%bcond_without check

# https://github.com/git-lfs/git-lfs
%global goipath         github.com/git-lfs/git-lfs
Version:                2.13.0

%gometa

%global gobuilddir %{_builddir}/%{name}-%{version}/_build

Name:           git-lfs
Release:        2%{?dist}
Summary:        Git extension for versioning large files

License:        MIT
URL:            https://git-lfs.github.io/
Source0:        https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-v%{version}.tar.gz
Source1:        README.Fedora

Provides:       bundled(golang(github.com/alexbrainman/sspi)) = 4729b3d4d8581b2db83864d1018926e4154f9406
Provides:       bundled(golang(github.com/avast/retry-go)) = a322e24d96313ab405dec28ad5711f036c6d25a3
Provides:       bundled(golang(github.com/davecgh/go-spew)) = 8991bc29aa16c548c550c7ff78260e27b9ab7c73
Provides:       bundled(golang(github.com/dpotapov/go-spnego)) = c2c609116ad04b25367e2789f273aaf8d1c263da
Provides:       bundled(golang(github.com/git-lfs/gitobj)) = cb39e779dc0b7949ea39318ff9345598d030d3b3
Provides:       bundled(golang(github.com/git-lfs/go-netrc)) = e0e9ca483a183481412e6f5a700ff20a36177503
Provides:       bundled(golang(github.com/git-lfs/go-ntlm)) = c5056e7fa0664ea69eed654a9618fa5e342dc347
Provides:       bundled(golang(github.com/git-lfs/wildmatch)) = 87c0f52cdf80ddb5b7de681298e5fee5898e8e16
Provides:       bundled(golang(github.com/hashicorp/go-uuid)) = 4f571afc59f3043a65f8fe6bf46d887b10a01d43
Provides:       bundled(golang(github.com/inconshreveable/mousetrap)) = 76626ae9c91c4f2a10f34cad8ce83ea42c93bb75
Provides:       bundled(golang(github.com/jcmturner/gofork)) = dc7c13fece037a4a36e2b3c69db4991498d30692
Provides:       bundled(golang(github.com/mattn/go-isatty)) = 6ca4dbf54d38eea1a992b3c722a76a5d1c4cb25c
Provides:       bundled(golang(github.com/olekukonko/ts)) = 78ecb04241c0121483589a30b0814836a746187d
Provides:       bundled(golang(github.com/pkg/errors)) = c605e284fe17294bda444b34710735b29d1a9d90
Provides:       bundled(golang(github.com/pmezard/go-difflib)) = 792786c7400a136282c1664665ae0a8db921c6c2
Provides:       bundled(golang(github.com/rubyist/tracerx)) = 787959303086f44a8c361240dfac53d3e9d53ed2
Provides:       bundled(golang(github.com/spf13/cobra)) = ef82de70bb3f60c65fb8eebacbb2d122ef517385
Provides:       bundled(golang(github.com/spf13/pflag)) = 298182f68c66c05229eb03ac171abe6e309ee79a
Provides:       bundled(golang(github.com/ssgelm/cookiejarparser)) = ead01002df3bee9f6d64bfde0c7387bd2368a2e0
Provides:       bundled(golang(github.com/stretchr/testify)) = 3ebf1ddaeb260c4b1ae502a01c7844fa8c1fa0e9
Provides:       bundled(golang(github.com/xeipuuv/gojsonpointer)) = 4e3ac2762d5f479393488629ee9370b50873b3a6
Provides:       bundled(golang(github.com/xeipuuv/gojsonreference)) = bd5ef7bd5415a7ac448318e64f11a24cd21e594b
Provides:       bundled(golang(github.com/xeipuuv/gojsonschema)) = 6b67b3fab74d992bd07f72550006ab2c6907c416
Provides:       bundled(golang(golang.org/x/crypto)) = a29dc8fdc73485234dbef99ebedb95d2eced08de
Provides:       bundled(golang(golang.org/x/net)) = 83d349e8ac1aeaa6e5b8669cbd094dccb2a7661d
Provides:       bundled(golang(golang.org/x/sync)) = 37e7f081c4d4c64e13b10787722085407fe5d15f
Provides:       bundled(golang(golang.org/x/sys)) = aee5d888a86055dc6ab0342f9cdc7b53aaeaec62
Provides:       bundled(golang(golang.org/x/text)) = f21a4dfb5e38f5895301dc265a8def02365cc3d0
Provides:       bundled(golang(gopkg.in/jcmturner/aescts.v1)) = f6abebb3171c4c1b1fea279cb7c7325020a26290
Provides:       bundled(golang(gopkg.in/jcmturner/dnsutils.v1)) = 13eeb8d49ffb74d7a75784c35e4d900607a3943c
Provides:       bundled(golang(gopkg.in/jcmturner/gokrb5.v5)) = 32ba44ca5b42f17a4a9f33ff4305e70665a1bc0
Provides:       bundled(golang(gopkg.in/jcmturner/rpc.v0)) = 4480c480c9cd343b54b0acb5b62261cbd33d7adf
Provides:       bundled(golang(gopkg.in/yaml.v2)) = 53403b58ad1b561927d19068c655246f2db79d48

# Generate man pages
BuildRequires:  /usr/bin/ronn


%if %{with check}
# Tests
BuildRequires:  perl-Digest-SHA
BuildRequires:  perl-Test-Harness
# Tests require full git suite, but not generally needed.
BuildRequires:  git >= 1.8.5
%endif

Requires:       git-core >= 1.8.5

%description
Git Large File Storage (LFS) replaces large files such as audio samples,
videos, datasets, and graphics with text pointers inside Git, while
storing the file contents on a remote server.


%prep
%autosetup -p1 -n %{name}-%{version}

install -m 0755 -vd %{gobuilddir}/bin
install -m 0755 -vd "$(dirname %{gobuilddir}/src/%{goipath})"
ln -fs "$(pwd)" "%{gobuilddir}/src/%{goipath}"

# Modify Makefile so that it expects binaries where we build them.
sed -i -e 's!\.\./bin/!/%{gobuilddir}/bin/!g' t/Makefile


%build
export GOPATH=%{gobuilddir}:%{gopath}
export GO111MODULE=off

# Build manpages first (some embedding in the executable is done.)
pushd docs
ronn --roff man/*.ronn
%gobuild -o %{gobuilddir}/bin/mangen man/mangen.go
%{gobuilddir}/bin/mangen
popd

%gobuild -o %{gobuilddir}/bin/git-lfs %{goipath}

# Build test executables
pushd %{gobuilddir}/src/%{goipath}
for cmd in t/cmd/*.go; do
    %gobuild -o "%{gobuilddir}/bin/$(basename $cmd .go)" "$cmd"
done
%gobuild -o "%{gobuilddir}/bin/git-lfs-test-server-api" t/git-lfs-test-server-api/*.go
popd

# Move man pages out of docs so they don't get installed twice.
mv docs/man .


%install
# In Fedora this is done by using %%gopkginstall
install -Dpm0755 %{gobuilddir}/bin/git-lfs %{buildroot}%{_bindir}/%{name}
install -d -p %{buildroot}%{_mandir}/man1/
install -Dpm0644 man/*.1 %{buildroot}%{_mandir}/man1/
install -d -p %{buildroot}%{_mandir}/man5/
install -Dpm0644 man/*.5 %{buildroot}%{_mandir}/man5/

%post
%{_bindir}/%{name} install --system --skip-repo

%preun
if [ $1 -eq 0 ]; then
    %{_bindir}/%{name} uninstall --system --skip-repo
fi
exit 0


%if %{with check}
%check
# In Fedora this is done by using %%gocheck
PATH=%{buildroot}%{_bindir}:%{gobuilddir}/bin:$PATH \
    make -C t PROVE_EXTRA_ARGS="-j$(getconf _NPROCESSORS_ONLN)"
%endif


%files
# In Fedora this is done by using %%gopkgfiles 
%doc README.md CHANGELOG.md docs
%license LICENSE.md
%{_bindir}/%{name}
%{_mandir}/man1/%%{name}*.1*
%{_mandir}/man5/%%{name}*.5*


%changelog
* Thu Apr 15 2021 Mohan Boddu <mboddu@redhat.com> - 2.13.0-2
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Wed Jan 06 2021 Ondřej Pohořelský <opohorel@redhat.com> - 2.13.0-1
- Enable bundling
- Update to latest version

* Mon Nov 09 2020 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.12.1-1
- Update to latest version (#1894780)

* Thu Sep 03 2020 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.12.0-1
- Update to latest version (#1874604)
- Remove duplicate docs/man directory (#1852765)
- Add an option to disable modifying the git filter config (#1768060)

* Sat Aug 01 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.11.0-3
- Second attempt - Rebuilt for
  https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Mon Jul 27 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.11.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Sun May 10 2020 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.11.0-1
- Update to latest version

* Thu Feb 20 2020 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.10.0-1
- Update to latest version

* Tue Jan 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Tue Jan 07 2020 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.9.2-1
- Update to latest version

* Wed Jan 01 2020 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.9.0-1
- Update to latest version

* Fri Aug 30 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.8.0-4
- Customize vendor information in version

* Fri Aug 30 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.8.0-3
- Update to latest version

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 2.7.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Wed Jul 10 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.7.2-2
- Update to latest Go macros

* Wed Apr 24 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.7.2-1
- Update to latest version

* Wed Feb 27 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.7.1-1
- Update to latest version

* Thu Feb 21 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.7.0-1
- Update to latest version

* Thu Jan 31 2019 Fedora Release Engineering <releng@fedoraproject.org> - 2.6.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Tue Jan 15 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.6.1-1
- Update to latest version

* Mon Jan 14 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.5.2-4
- Rebuilt for dependencies

* Tue Oct 23 2018 Nicolas Mailhot <nim@fedoraproject.org>
- 2.5.2-3
- redhat-rpm-config-123 triggers bugs in gosetup, remove it from Go spec files as it’s just an alias
- https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/message/RWD5YATAYAFWKIDZBB7EB6N5DAO4ZKFM/

* Fri Oct 12 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.5.2-2
- rebuilt

* Wed Oct 10 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.5.2-1
- Update to latest version

* Tue Sep 04 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.5.1-1
- Update to latest version

* Mon Sep 03 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.5.0-1
- Update to 2.5.0

* Wed Aug 29 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.4.2-1
- Update to 2.4.2

* Tue Jul 31 2018 Florian Weimer <fweimer@redhat.com> - 2.4.1-3
- Rebuild with fixed binutils

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.4.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Wed May 23 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.4.1-1
- Update to latest release

* Mon May 21 2018 Carl George <carl@george.computer> - 2.4.0-3
- Fix %%preun to correctly remove the lfs filter on uninstall (rhbz#1580357)

* Mon Mar 12 2018 Carl George <carl@george.computer> - 2.4.0-2
- Add %%go_arches fallback to work around Koji issues

* Sun Mar 04 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.4.0-1
- Update to latest release.

* Thu Feb 08 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.3.4-6
- Add patches to build with Go 1.10.

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.3.4-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Dec 04 2017 Carl George <carl@george.computer> - 2.3.4-4
- Use vendored libraries on RHEL
- Skip test on RHEL
- Don't build man pages on RHEL due to missing ronn
- Don't build html versions of man pages

* Fri Dec 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.3.4-3
- Require git-core instead of git.

* Fri Nov 03 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.3.4-2
- Patch tests to work on slow systems like arm and aarch builders.
- Fix "git lfs help" command.

* Fri Nov 03 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.3.4-1
- Update to latest release.
- Run all tests during build.

* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.2.1-3
- Remove redundant doc tag on manpages.
- Use path macros in %%post/%%postun.

* Thu Aug 31 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.2.1-2
- Disable unnecessary subpackages.

* Sun Jul 30 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.2.1-1
- Update to latest version.

* Wed Apr 19 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.2-2
- Patch up to build with Go 1.7

* Wed Apr 19 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.2-1
- Update to latest release
- Add some requested macros

* Tue Mar 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.1-1
- Update to latest release
- Don't disable git-lfs globally during upgrade

* Mon Mar 06 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0-1
- Update to latest release

* Sun Feb 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.5.5-1
- Update to latest release
- Add -devel and -unit-test-devel subpackages
- Add post/preun scriptlets for global enablement

* Sun May 15 2016 Igor Gnatenko <i.gnatenko.brain@gmail.com> - 1.2.0-1
- Initial package
